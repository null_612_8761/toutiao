var childProcess = require('child_process');
var { createArticleImage, delDir, createDir } = require('./config')
var args = process.argv.splice(2) // 变量值
if (args.length && args[0] === 'pro') {
    process.env.AddressType = 1 // 指定模型的地址类型
}

 console.log('开始删除上传文件目录')
 delDir('./public/uploads')
 createDir('./public/uploads')

 console.log('删除上传文件目录成功')
 childProcess.exec(" mongorestore -h 127.0.0.1:27017 -d  toutiao ./src/doc/database --drop",  function (error, stdout, stderr) {
  if (error) {
      console.log("执行重置数据库失败, 异常信息:" + error.message)
  }else {
      console.log("执行数据库重置成功！")
       createArticleImage()
  }
})

