/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "article_id" : { type: String },
	"content" : { type: String },
    "liking_id" : { type: String },
	"user_id" : { type: String },
	"comment_id" : { type: String },
	"create_time" : { type: String,  default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"is_deleted" : { type: String },
	"update_time" : { type: String,  default: () =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
})
var CommentLiking = mongoose.model('news_comment_liking', schema, 'news_comment_liking'); 

module.exports = CommentLiking;