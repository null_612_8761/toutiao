/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
     "intro": { type: String },
    "images" : { type: Array },
    "user_id" : { type: String },

	"create_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
})
var UserFeed = mongoose.model('user_feed', schema, 'user_feed'); 

module.exports = UserFeed