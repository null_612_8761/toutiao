/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "article_id" : { type: String },
	"attitude" : { type: Number, default: -1 },
	"user_id" : { type: String },
	"create_time" : { type: String,  default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
})
var ArticleAttitude = mongoose.model('news_attitude', schema, 'news_attitude'); 

module.exports = ArticleAttitude;