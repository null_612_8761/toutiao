/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
	"user_id" : { type: String },
	"mobile" : { type: String },
	"password" : { type: String },
	"user_name" : { type: String },
	"profile_photo" : { type: String },
	"last_login" : { type: String },
	"is_media" : { type: String },
	"article_count" : { type: Number , default: 0},
	"following_count" :  { type: Number, default: 0 },
	"fans_count" : { type: Number, default: 0 },
	"like_count" : { type: Number, default: 0 },
	"read_count" : { type: Number , default: 0},
	"introduction" : { type: String },
	"certificate" : { type: String },
	"is_verified" : { type: String },
	"account" : { type: String },
	"email" : { type: String },
	"status" :  { type: String }
})
var UserBasic = mongoose.model('user_basic', schema, 'user_basic'); 

module.exports = UserBasic;