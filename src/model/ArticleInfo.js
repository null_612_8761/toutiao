/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')

var schema = new mongoose.Schema({
    "article_id" : { type: String },
	"content" : { type: String },
})
var Article = mongoose.model('news_article_content', schema, 'news_article_content'); 

module.exports = Article;