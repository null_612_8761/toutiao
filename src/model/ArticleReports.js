/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "article_id" : { type: String },
	"content" : { type: String },
    "report_id" : { type: String },
	"user_id" : { type: String },
	"article_id" : { type: String },
	"type" : { type: String },
	"remark" : { type: String },
	"create_time" : { type: String, default: () =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss')  }
})
var ArticleReport = mongoose.model('news_report', schema, 'news_report'); 

module.exports = ArticleReport