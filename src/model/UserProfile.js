/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "user_id" : { type: String },
	"gender" : { type: Number },
	"birthday" : { type: String },
	"real_name" : { type: String },
	"create_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss')  },
	"update_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"register_media_time" : { type: String },
	"id_number" : { type: String },
	"id_card_front" : { type: String },
	"id_card_back" : { type: String },
	"id_card_handheld" : { type: String },
	"area" : { type: String },
	"company" : { type: String },
	"career" : { type: String }
})
var UserProfile = mongoose.model('user_profile', schema, 'user_profile'); 

module.exports = UserProfile;