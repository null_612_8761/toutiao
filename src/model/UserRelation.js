/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "relation_id" : { type: String },
	"user_id" : { type: String },
	"target_user_id" : { type: String },
	"relation" :{ type: String },
	"create_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"update_time" : { type: String , default:() => sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') }
})
var UserRelation = mongoose.model('user_relation', schema, 'user_relation'); 

module.exports = UserRelation