/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "collection_id" : { type: String },
	"user_id" : { type: String },
	"article_id" : { type: String },
	"create_time" : { type: String,  default: () => sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"is_deleted" : { type: String },
	"update_time" : { type: String,  default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') }
})
var ArticleCollect = mongoose.model('news_collection', schema, 'news_collection'); 

module.exports = ArticleCollect;