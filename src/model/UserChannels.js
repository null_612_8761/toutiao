/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "user_channel_id" :{ type: String },
	"user_id" : { type: String },
	"channel_id" : { type: String },
	"create_time" : { type: String, default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss')  },
	"is_deleted" : { type: String },
	"update_time" : { type: String },
	"sequence" :{ type: Number }
})
var UserChannel = mongoose.model('news_user_channel', schema, 'news_user_channel'); 

module.exports = UserChannel;