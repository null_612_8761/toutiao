/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
	"mobile" : { type: String },
	"code" :{ type: String },
	"create_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
     "timer": { type: Number } // 时间戳
})
var UserCode = mongoose.model('user_code', schema, 'user_code'); 

module.exports = UserCode