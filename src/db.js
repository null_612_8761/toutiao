/*!
 * Koa CMS Backstage management
 *
 * Copyright JS shuiruohanyu
 * Released under the ISC license
 *
 */
const mongoose = require('mongoose');
const db = mongoose.connect("mongodb://localhost:27017/toutiao",{ 
    useNewUrlParser: true, 
    useCreateIndex : true,
    useUnifiedTopology: true,
    poolSize: 100,
    useFindAndModify: false }, function (error) {
   if (error) {
       console.log("连接本地mongo数据库失败:" + error.message )
   }else {
    console.log("连接本地mongo数据库成功")

   }
});
module.exports = db;