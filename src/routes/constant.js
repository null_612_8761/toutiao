const address = require('../../address')
const addressPath = process.env.AddressType == 1 ? address.address : `${address.host}:${address.port}`

module.exports = {
    defaultHeadImg: `http://${addressPath}/images/user_head.jpg`,
    prefixImage: 'http://toutiao-img.itheima.net/',
    blackList: ['user/followings', 'user' ] // 黑名单url地址 在该名单里面的 需要 进行token认证
}