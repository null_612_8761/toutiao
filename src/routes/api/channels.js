const router = require("koa-router")();


const ChannelModel = require('../../model/Channels')

// 获取所有频道
router.get('/', async (ctx, next) => {
    const allChannels = await ChannelModel.find({}) // 所有频道
    const channels = [{ "id": 0, "name": "推荐" }]
    // 如果用户登录 返回用户的频道数据 
    channels.push(...allChannels.map(item => ({ id: parseInt(item.channel_id)  , name: item.channel_name })))  
    ctx.status = 200
    return {  data: { channels } }
    // 如果用户未登录 返回默认的频道数据
})
module.exports = router.routes();
