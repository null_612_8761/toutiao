const UserLogin = require('../model/UserLogin')
const ArticleReports = require('../model/ArticleReports')
const { checkHttp } = require('../utils/validate')
const { defaultHeadImg, prefixImage } = require('./constant')
const UUID = require('uuid')
const mongoose = require('mongoose');

const timeOut = 5400 // token有效期 一个半小时
module.exports = {
    // 创建token
    async createTokenByUserId (user_id) {
        // 只要是重新登录 则不论之前的数据 重新生成
      const loginP = await UserLogin.findOne({ user_id }).lean()
      const token = UUID.v4()
      const refresh_token = UUID.v4()
      const tokenObj = { token, refresh_token , timer:  new Date().getTime() } 
      if (loginP) {
        // 如果找到了 就再次延长之前的登录时间
        await UserLogin.findOneAndUpdate({ user_id }, { ...loginP, timer:  new Date().getTime() })
        // 找到token之后 返回之前的token
        return { token: loginP.token,  refresh_token: loginP.refresh_token  }
      }else {
        await UserLogin.create({ user_id, ...tokenObj })
      }
      return { token, refresh_token   }
    },
    // 检查token过期时间 返回true 不过期 返回false过期
    async checkTokenByContext (ctx) {
        let { authorization } = ctx.request.header
        if (!authorization) {
            return false
        }
        const token = authorization.split(' ')[1]
        const loginP = await UserLogin.findOne({ token }).lean()
        if (loginP) { 
          return (new Date().getTime()  - parseInt(loginP.timer)) / 1000 <  timeOut
        }
        return false
    },
    // 根据上下文获取用户id
    async getUserIdByContext (ctx) {
        let { authorization } = ctx.request.header
        if (!authorization) {
            return null
        }
        const token = authorization.split(' ')[1]
        const loginP = await UserLogin.findOne({ token }).lean()
        return loginP ? loginP.user_id : null
    },
    // 根据token获取用户id
    async getUserIdByToken (token) {
        const loginP = await UserLogin.findOne({ token }).lean()
        return loginP ? loginP.user_id : null
    },
    // 根据刷新token换取新token
    async createNewTokenByContext (ctx) {
        let { authorization } = ctx.request.header
        if (!authorization) {
            throw new Error('refresh_token未传')
        }
       const refresh_token = authorization.split(' ')[1]
       const  loginP = await UserLogin.findOne({ refresh_token }).lean()
       let token = UUID.v4()
       if (loginP) {
            // 找到了登录信息 生成新的
            await UserLogin.findOneAndUpdate({ refresh_token }, { token, timer: new Date().getTime() })
       }else {
         throw new Error('refresh_token失效')
       }
       return  token
    },
     // 获取黑名单列表
    async getBlackArticleIdList (user_id) {
       const list = await ArticleReports.find({ user_id  }).lean()
       return list.map(item => item.article_id)
    },
    // 获取图片地址
   getImage (value) {
       if (value) {
         if (checkHttp(value)) {
           return value
         }
         return prefixImage + '/' + value
       }
       return defaultHeadImg
    },
    // 获取默认人的头像和身份
    getDefaultMan () {
      return {
        aut_id: 1111,
        aut_name: '黑马先锋',
        aut_photo: defaultHeadImg
      }
    }
}