/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email swh1057607246@qq.com
 *
 */
process.env.AddressType = 1 // 设置生产环境变量

const Koa = require("koa"),
  Router = require("koa-router"),
  Static = require("koa-static"),
  // Session = require("koa-session"),
  BodyParser = require("koa-bodyparser"),
  path = require("path"),
  compress = require("koa-compress"),
  jsonp = require("koa-jsonp"),
  json = require("./src/routes/json"),
  api = require("./src/routes/api"), // 后端接口
  url = require("url"),
  opn = require('opn'),
  cors = require('koa2-cors'),
  address = require('./address'),
  service = require('./src/bury'),
  { blackList } = require('./src/routes/constant'),
  { checkTokenByContext } = require('./src/routes/token')
  require('./src/db') // 引入数据库
  require("./src/routes/websocket")
// 初始化web服务
const app = new Koa();
var isBuryStart = false

const router = new Router();
//配置session
app.keys = ["some secret hurr"];
app.use(cors()); // 注册使用跨域中间件

// app.use(
//   Session({
//       key: "koa:sess",
//       maxAge: 5400000,
//       overwrite: true,
//       httpOnly: true,
//       signed: true,
//       rolling: true,
//       renew: false,
//     },app)
// );
// 注册redis的中间件
// app.use(koaSession({
//   store: new Redis() // 实例化redis
// }));
//配置静态资源
app.use(Static(path.join(__dirname, "public")));
app.use(Static(path.join(__dirname, "statics")));
//配置post请求数据接收
app.use(BodyParser());
//jsonp
app.use(jsonp());

//gzipd
app.use(
  compress({
    filter: function (content_type) {
      return true;
    },
    threshold: 2048,
    flush: require("zlib").Z_SYNC_FLUSH,
  })
);
// //全局属性
app.use(async (ctx, next) => {
  // if (!isBuryStart) {
  //   // 只记录第一次埋点
  //   isBuryStart = true
  //   service.bury(ctx, 'toutiao')
  // }
  const prefix = '/v1_0/'
  var pathname = url.parse(ctx.url).pathname;
   if (!blackList.some(item => (prefix + item) === pathname )) {
      // 如果在白名单里面 就放过 不检查token有效性
     await next()
   }else {
    const isOut = await checkTokenByContext(ctx) // 是否超时
    if (isOut) {
      await next()
    }else {
      ctx.status = 401 // 超时token
      ctx.body = { message: 'token超时或者未传token' }
    }
   }
});

app.use(async (ctx, next) => {
  try {
    const data =  await next() // 获取接口返回数据
    if (data && !data.success && data.status)  {
      ctx.status = data.status
    }
    ctx.body = { ...json, ...data, status: undefined, success: undefined }
     // 进行数据埋点  记录操作的信息
  } catch (error) {
    // 进行数据埋点  记录操作的信息
    service.bury(ctx, 'toutiao', error.message)
    console.log( "执行接口出现异常:" + error.message)
    ctx.status = 500
    ctx.body = {
       message: error.message
    }
  }

})


// 启动入口
// 保证加不加app都可以正常启动
router.use('/v1_0', api)

// router.use
// router.use('/app/v1_1/articles', article)
// router.use('/v1_0', api)
// 保证加不加app都可以正常启动
app.use(router.routes())

//启动路由
app.use(router.allowedMethods());
app.use(async ctx => {
   if(ctx.status ===  404) {
     console.log("找不到接口:" + ctx.originalUrl)
     service.bury(ctx, 'toutiao', '找不到接口地址:' + ctx.originalUrl)  // 记录地址错误的问题
     ctx.status = 404
     ctx.body = {
       message: '您请求的接口还未实现, 请检查您的请求路径是否正确!'
     }
   }
})

//启动服务器
app.listen(address.port, (err) =>{
  console.log(`黑马头条后端接口启动, http://${address.host}:${address.port}`)
   opn(`http://${address.host}:${address.port}/index.html`)
});
