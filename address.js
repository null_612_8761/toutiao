const os = require('os');
const getIPAdress = () => {
    var interfaces = os.networkInterfaces();
    for (var devName in interfaces) {
        var iface = interfaces[devName];
        for (var i = 0; i < iface.length; i++) {
            var alias = iface[i];
            if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
                return alias.address;
            }
        }
    }
}
module.exports = {
    host: getIPAdress(), // 域名
    port: 8000,// 端口
    address: 'geek.itheima.net' // 域名 当 npm run pro时 读取此值
}